<?php

namespace Drupal\Tests\required_by_role\Unit;

use Drupal\Core\Session\AccountInterface;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the required_by_role plugin.
 *
 * @group required_by_role
 *
 * @see \Drupal\required_by_role\Plugin\Required\RequiredByRole
 */
class RequiredByRoleTest extends UnitTestCase {

  /**
   * The required plugin.
   *
   * @var \Drupal\required_by_role\Plugin\Required\RequiredByRole
   */
  protected $plugin;

  /**
   * Method getInfo.
   *
   * @return array
   *   Information regarding the tests.
   */
  public static function getInfo() {
    return [
      'name' => 'Required by role plugin',
      'description' => 'Test the required by role logic.',
      'group' => 'Required API',
    ];
  }

  /**
   * Caching the plugin instance in the $plugin property.
   */
  public function setUp(): void {
    parent::setUp();
    $this->plugin = $this->getMockBuilder('Drupal\required_by_role\Plugin\Required\RequiredByRole')
      ->disableOriginalConstructor()
      ->onlyMethods([])
      ->getMock();
  }

  /**
   * Tests the required by role behavior.
   *
   * @dataProvider getRequiredCases
   */
  public function testRequiredByRole($result, $user_roles, $required_roles) {
    $required = $this->plugin->getMatches($user_roles, $required_roles);
    $this->assertEquals($result, $required);
  }

  /**
   * Provides a cases to test.
   */
  public static function getRequiredCases() {
    return [
      // User with matching roles.
      [
        TRUE,
        [
          AccountInterface::AUTHENTICATED_ROLE,
          'administrator',
        ],
        [
          'administrator',
        ],
      ],
      // User with no matching roles.
      [
        FALSE,
        [
          AccountInterface::AUTHENTICATED_ROLE,
          'administrator',
        ],
        [
          AccountInterface::ANONYMOUS_ROLE,
        ],
      ],
      // No required roles set.
      [
        FALSE,
        [
          AccountInterface::AUTHENTICATED_ROLE,
          'administrator',
        ],
        [],
      ],
    ];
  }

}
