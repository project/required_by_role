<?php

namespace Drupal\required_by_role\Plugin\Required;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\required_api\Plugin\Required\RequiredBase;
use Drupal\user\Entity\Role;

/**
 * Required by role plugin.
 *
 * @Required(
 *   id = "required_by_role",
 *   admin_label = @Translation("Required by role"),
 *   label = @Translation("Required by role"),
 *   description = @Translation("Required based on current user roles.")
 * )
 */
class RequiredByRole extends RequiredBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function isRequired(FieldDefinitionInterface $field, AccountInterface $account) {
    $available_roles = $account->getRoles();
    $field_roles = $field->getThirdPartySetting('required_api', 'required_plugin_options', []);

    return $this->getMatches($available_roles, $field_roles);
  }

  /**
   * Helper method to test if the role exists into the allowed ones.
   *
   * @param array $user_roles
   *   Roles belonging to the user.
   * @param array $required_roles
   *   Roles that are required for this field.
   *
   * @return bool
   *   Whether the user have a required role or not.
   */
  public function getMatches(array $user_roles, array $required_roles) {
    $match = array_intersect($user_roles, $required_roles);

    return !empty($match);
  }

  /**
   * Form element to build the required property.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field
   *   The field instance.
   *
   * @return array
   *   Form element
   */
  public function requiredFormElement(FieldDefinitionInterface $field) {
    $default_value = $field->getThirdPartySetting('required_api', 'required_plugin_options') ?: [];

    $options = array_map(
      function ($role) {
        return ['name' => $role->label()];
      },
      Role::loadMultiple()
    );
    unset($options[AccountInterface::AUTHENTICATED_ROLE]);

    return [
      '#type' => 'tableselect',
      '#header' => [
        'name' => ['data' => $this->t('Role')],
      ],
      '#options' => $options,
      '#default_value' => $default_value,
      '#js_select' => TRUE,
      '#multiple' => TRUE,
      '#empty' => $this->t('No roles available.'),
      '#attributes' => [
        'class' => ['tableselect-required-by-role'],
      ],
    ];
  }

}
